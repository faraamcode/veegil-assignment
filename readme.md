# VEEGIL ASSIGNMENT SUBMITTED BY IBRAHIM ABDULRASAQ KAYODE

## VEEGIL BANK APPLICATION (RESTAPI)

This is web application written with node js. This backend make up of different route which will communicate with the frontend.

## this application is hosted on the heroku

(link)[https://veegil-bank.herokuapp.com/]

# Installation

> you will clone or download the source code to your computer
> install the depencies

```
npm install
```

1.  Install postgre database
2.  create a database (desired database name) and run the app.sql file as SQL Querries.
3.  create an .env file to save you environment variables
4.  start the application, it will run on port 3002

```
npm start
```

# Routes

```

http://localhost:3002/find
http://localhost:3002/signup
http://localhost:3002/login
http://localhost:3002/test

http://localhost:3002/transaction/withdraw
http://localhost:3002/transaction/deposit
http://localhost:3002/transaction/history
http://localhost:3002/transaction/paginate

```
