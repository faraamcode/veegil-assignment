CREATE DATABASE bank;
CREATE TABLE customer_tbl ( id BIGSERIAL NOT NULL PRIMARY KEY,
 fullname VARCHAR(255) NOT NULL,
 phone_number VARCHAR(255) NOT NULL,
 email VARCHAR(255) NOT NULL,
 date_became_customer TIMESTAMP NOT NULL,
 username VARCHAR(255) NOT NULL,
 password VARCHAR(255) NOT NULL,
 gender VARCHAR(255) NOT NULL,
 date_of_birth DATE NOT NULL,
 passport VARCHAR(255) NOT NULL,
 next_of_kin_name VARCHAR(255) NOT NULL,
 next_of_kin_email VARCHAR(255) NOT NULL,
 next_of_kin_phone VARCHAR(255) NOT NULL,
 account_type VARCHAR(255) NOT NULL,
 balance  numeric(20,2) NOT NULL,
 last_login TIMESTAMP 

 );
CREATE TABLE transaction_tbl ( id BIGSERIAL NOT NULL PRIMARY KEY,
 time_of_transaction TIMESTAMP NOT NULL,
 phone_number VARCHAR(255) NOT NULL,
 amount  numeric(20,2) NOT NULL,
 transaction_type VARCHAR(255) NOT NULL,
 other_details TEXT NOT NULL,
    previous_balance numeric(20,2) NOT NULL,
 current_balance numeric(20,2) NOT NULL
 );
