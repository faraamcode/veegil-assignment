const router = require("express").Router();
const controler = require("./transaction.controller");
const { verifyUserToken } = require("../utils/verify.function");
const { loadTables } = require("../utils/seed");
// transactions can be deposit or withdrawal which means the amoount to be sent on the body (-amounnt for withdrawal) and (+ amount for deposit)
router.post(
  "/transaction/withdraw",
  verifyUserToken,
  controler.Withdrawal,
  controler.SaveTransaction
);
router.post(
  "/transaction/deposit",
  verifyUserToken,
  controler.Deposit,
  controler.SaveDeposit
);
router.get("/seed", loadTables);
router.post("/transaction/history", controler.getHistory);
router.post("/transaction/history/paginate", controler.getHistoryPaginate);
module.exports = router;
