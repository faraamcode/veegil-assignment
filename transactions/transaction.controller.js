const express = require("express");
const Transaction = require("../utils/transaction.query");
exports.Withdrawal = async (req, res, next) => {
  const amount = req.body.amount;
  const uniqueValue = [req.body.phone_number];
  const result = await Transaction.DeductCustomerBanlance(amount, uniqueValue);
  if (result.length > 0) {
    req.body.UserDetails = result;
    next();
  } else if (result === false) {
    res.status(404).json({
      message: "insufficient balance",
    });
  } else {
    res.status(403).json({
      message: "transaction failed",
    });
  }
};
exports.Deposit = async (req, res, next) => {
  const amount = req.body.amount;
  const uniqueValue = [req.body.phone_number];
  const result = await Transaction.AddCustomerBanlance(amount, uniqueValue);
  if (result.length > 0) {
    req.body.UserDetails = result;
    next();
  } else {
    res.status(403).json({
      message: "transaction failed",
    });
  }
};
exports.SaveTransaction = async (req, res, next) => {
  const data = req.body.UserDetails;
  const currentBalance = req.body.UserDetails[0].balance - req.body.amount;
  const value = [
    req.body.time_of_transaction,
    req.body.phone_number,
    req.body.amount,
    req.body.transaction_type,
    req.body.other_details,
    req.body.UserDetails[0].balance,
    currentBalance,
  ];

  const result = await Transaction.InsertNewField(value);
  if (result.rowCount === 1) {
    res.status(201).json({ message: "Withdrawal Successfully" });
  } else {
    res.status(201).json({ message: "Transaction Failed" });
  }
};
exports.SaveDeposit = async (req, res, next) => {
  const data = req.body.UserDetails;
  const amount = parseFloat(req.body.amount).toFixed(2);

  const currentBalance =
    parseFloat(req.body.UserDetails[0].balance) + parseFloat(req.body.amount);
  console.log(currentBalance);
  const value = [
    req.body.time_of_transaction,
    req.body.phone_number,
    req.body.amount,
    req.body.transaction_type,
    req.body.other_details,
    req.body.UserDetails[0].balance,
    currentBalance,
  ];

  const result = await Transaction.InsertNewField(value);
  if (result.rowCount === 1) {
    res.status(201).json({ message: "Deposit Successfully" });
  } else {
    res.status(201).json({ message: "Transaction Failed" });
  }
};

exports.getHistory = async (req, res, next) => {
  const uniqueValue = [req.body.phone_number];
  const response = await Transaction.fetchByUniqueFieldDesc(uniqueValue);
  const result = response.rows;
  const count = response.rowCount;
  res.status(200).json({ count, result });
};
exports.getHistoryPaginate = async (req, res, next) => {
  const uniqueValue = [req.body.phone_number];
  const page = req.body.page;
  const response = await Transaction.fetchByUniqueFieldPagination(
    uniqueValue,
    page
  );
  const result = response;
  res.status(200).json(result);
};
