const express = require("express");
const cors = require("cors");
require("dotenv").config();
const customerRoute = require("./customer/customer.route");
const transactionRoute = require("./transactions/transaction.route");

const app = express();
app.use(express.json());
app.use(cors());
app.use(customerRoute);
app.use(transactionRoute);
app.listen(process.env.PORT || 3002);
