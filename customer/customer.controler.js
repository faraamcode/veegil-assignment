const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Customer = require("../utils/customer.query");

exports.fetchAll = async (req, res, next) => {
  const result = await Customer.fetchAll();
  res.status(200).json(result);
};

exports.fetchByUniqueID = async (req, res, next) => {
  const uniqueValue = [req.body.phone_number];
  const response = await Customer.fetchByUniqueField(uniqueValue);
  const result = response.rowCount;
  if (result > 0) {
    res.status(400).json({ message: "user already exist", account: false });
  } else {
    next();
  }
};
exports.getUserdetails = async (req, res, next) => {
  const uniqueValue = [req.body.phone_number];
  const response = await Customer.fetchByUniqueField(uniqueValue);
  const result = response.rows[0];
  res.status(200).json(result);
  // if (result > 0) {
  //   res.status(400).json({ message: "user already exist", account: false });
  // } else {
  //   next();
  // }
};

exports.CustomerLogin = async (req, res, next) => {
  const uniqueValue = [req.body.phone_number];
  const response = await Customer.fetchByUniqueField(uniqueValue);
  if (response.rows.length > 0) {
    const check = response.rows[0];
    const dbpassword = check.password;
    const verifyPassword = await bcrypt.compare(req.body.password, dbpassword);
    if (!verifyPassword) {
      res.status(401).json({
        message: "invalid password",
        access: false,
      });
    } else {
      jwt.sign(
        { check },
        process.env.MY_TOKEN_SECRETE,
        { expiresIn: 60 * 60 * 2 },
        (err, token) => {
          if (!err)
            res.status(200).json({
              access: true,
              token,
              userDetails: check,
              message: "user login successfully",
            });
        }
      );
    }
  } else {
    res.status(403).json({
      access: false,
      message: "account does not exist",
    });
  }
};

exports.postAll = async (req, res, next) => {
  const {
    fullname,
    phone_number,
    email,
    date_became_customer,
    username,
    password,
    gender,
    date_of_birth,
    passport,
    next_of_kin_name,
    next_of_kin_email,
    next_of_kin_phone,
    account_type,
    balance,
    last_login,
  } = req.body;
  const hashed = await bcrypt.hash(password, 10);
  //   console.log(hashed);
  //   const check = await bcrypt.compare(password, hashed);
  //   console.log(check);
  const values = [
    fullname,
    phone_number,
    email,
    date_became_customer,
    username,
    hashed,
    gender,
    date_of_birth,
    passport,
    next_of_kin_name,
    next_of_kin_email,
    next_of_kin_phone,
    account_type,
    balance,
    last_login,
  ];
  // console.log(values);
  const result = await Customer.InsertNewField(values);
  // console.log(result.Result);
  if (result.rowCount === 1) {
    res.status(200).json({
      account: true,
      message: "account created succesfully",
    });
  } else {
    res.status(400).json({
      account: false,
      message: "account not created",
    });
  }
};
