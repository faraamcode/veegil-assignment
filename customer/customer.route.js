const router = require("express").Router();
const conttoller = require("./customer.controler");
const { testUserToken } = require("../utils/verify.function");
const { route } = require("../transactions/transaction.route");
router.post("/find", conttoller.getUserdetails);
router.post("/signup", conttoller.fetchByUniqueID, conttoller.postAll);
router.post("/login", conttoller.CustomerLogin);
router.post("/test", testUserToken);
module.exports = router;
