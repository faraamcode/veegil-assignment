const express = require("express");
const pool = require("../db/connect");
const Query = require("./queries");
class Customer extends Query {
  constructor(tablename, fieldname, uniqueField) {
    super(tablename, fieldname, uniqueField);
  }
}

const fields = [
  "fullname",
  "phone_number",
  "email",
  "date_became_customer",
  "username",
  "password",
  "gender",
  "date_of_birth",
  "passport",
  "next_of_kin_name",
  "next_of_kin_email",
  "next_of_kin_phone",
  "account_type",
  "balance",
  "last_login",
];
const uniqueField = ["phone_number"];
module.exports = new Customer("customer_tbl", fields, uniqueField);
