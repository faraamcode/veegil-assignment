const express = require("express");
const pool = require("../db/connect");
const Query = require("./queries");
class Transaction extends Query {
  constructor(tablename, fieldname, uniqueField) {
    super(tablename, fieldname, uniqueField);
  }
  async DepositFund(amount) {}
}

const fields = [
  "time_of_transaction",
  "phone_number",
  "amount",
  "transaction_type",
  "other_details",
  "previous_balance",
  "current_balance",
];
const uniqueField = ["phone_number"];
module.exports = new Transaction("transaction_tbl", fields, uniqueField);
