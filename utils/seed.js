const express = require("express");
const pool = require("../db/connect");

exports.loadTables = async (req, res, next) => {
  const result = await pool.query(
    "CREATE TABLE transaction_tbl ( id BIGSERIAL NOT NULL PRIMARY KEY,time_of_transaction TIMESTAMP NOT NULL, phone_number VARCHAR(255) NOT NULL, amount  numeric(20,2), transaction_type VARCHAR(255) NOT NULL, other_details TEXT NOT NULL, previous_balance numeric(20,2), current_balance numeric(20,2))"
  );
  const result2 = await pool.query(
    "CREATE TABLE customer_tbl ( id BIGSERIAL NOT NULL PRIMARY KEY,fullname VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, date_became_customer TIMESTAMP NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, date_of_birth DATE NOT NULL, passport VARCHAR(255) NOT NULL, next_of_kin_name VARCHAR(255) NOT NULL, next_of_kin_email VARCHAR(255) NOT NULL, next_of_kin_phone VARCHAR(255) NOT NULL, account_type VARCHAR(255) NOT NULL, balance  numeric(20,2), last_login TIMESTAMP );"
  );
  if (result && result2) {
    res.status(200).json({
      message: "table created",
    });
  } else {
    res.status(500).json({
      message: "table not created",
    });
  }
};
