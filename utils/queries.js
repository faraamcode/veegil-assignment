const express = require("express");
const pool = require("../db/connect");
module.exports = class Query {
  constructor(tablename, fieldname, uniqueField) {
    this.tablename = tablename;
    this.fieldname = fieldname;
    this.uniqueField = uniqueField;
  }

  //  fetch all in the database
  async fetchAll() {
    this.result = await pool.query(`SELECT * FROM ${this.tablename}`);
    console.log(this.tablename);
    console.log(this.fieldname);
    this.data = await this.result.rows;
    return this.data;
  }

  async InsertNewField(values) {
    this.values = values;
    this.fieldvalueNumber = this.values
      .map((items, index) => {
        return `$${index + 1}`;
      })
      .join(", ");

    this.result = await pool.query(
      `INSERT INTO ${this.tablename} (${this.fieldname}) VALUES (${this.fieldvalueNumber})`,
      this.values
    );
    return this.result;
  }

  static turnForUpdate() {
    this.uniqueField = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join("");
    console.log(this.uniqueField);
  }

  async fetchByUniqueField(uniqueValue) {
    this.uniqueValue = uniqueValue;

    this.uniquefieldtoused = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join(" ");
    this.result = await pool.query(
      `SELECT * FROM ${this.tablename} WHERE ${this.uniquefieldtoused}`,
      this.uniqueValue
    );
    // console.log(this.result);
    // console.log(this.uniquefieldtoused, this.uniqueValue);

    return this.result;
  }

  async fetchByUniqueFieldDesc(uniqueValue) {
    this.uniqueValue = uniqueValue;

    this.uniquefieldtoused = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join(" ");
    this.result = await pool.query(
      `SELECT * FROM ${this.tablename} WHERE ${this.uniquefieldtoused} ORDER BY time_of_transaction DESC`,
      this.uniqueValue
    );
    // console.log(this.result);
    // console.log(this.uniquefieldtoused, this.uniqueValue);

    return this.result;
  }

  async fetchByUniqueFieldPagination(uniqueValue, page) {
    this.page = page * 10;
    this.uniqueValue = uniqueValue;
    this.uniquefieldtoused = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join(" ");
    this.count = await pool.query(
      `SELECT COUNT(id) FROM ${this.tablename} WHERE ${this.uniquefieldtoused}`,
      this.uniqueValue
    );
    // console.log(this.page);
    this.result = await pool.query(
      `SELECT * FROM ${this.tablename} WHERE ${this.uniquefieldtoused} ORDER BY time_of_transaction DESC LIMIT 10 OFFSET ${this.page}`,
      this.uniqueValue
    );
    this.response = {
      page: this.count.rows[0].count,
      result: this.result.rows,
    };
    console.log(this.response);
    // console.log(this.uniquefieldtoused, this.uniqueValue);

    return this.response;
  }

  async DeductCustomerBanlance(amount, uniqueValue) {
    this.uniqueValue = uniqueValue;
    this.amount = amount;
    this.uniquefieldtoused = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join(" ");
    //    checking the account no if it si available
    this.account = await pool.query(
      `SELECT * FROM customer_tbl WHERE ${this.uniquefieldtoused}`,
      this.uniqueValue
    );
    this.accountData = this.account.rows;
    // checking withdrawal possibilty l.e if the available balance can undergo the amount deduction
    this.possible = this.accountData[0].balance - amount;
    if (this.possible < 0) {
      return false;
    }
    // console.log(this.possible);
    // if account is available the deduction will be made to the account
    if (this.accountData.length > 0) {
      this.response = await pool.query(
        `UPDATE customer_tbl SET balance = balance - ${this.amount} WHERE ${this.uniquefieldtoused}`,
        this.uniqueValue
      );
      //    check if the account has been deducted succefully
      if (this.response.rowCount === 1) {
        //   insertind records into the transaction table
        return this.accountData;
      }
    }
    // return this.response;
  }

  async AddCustomerBanlance(amount, uniqueValue) {
    this.uniqueValue = uniqueValue;
    this.amount = amount;
    this.uniquefieldtoused = this.uniqueField
      .map((item, index) => `${item} = $${index + 1}`)
      .join(" ");
    //    checking the account no if it si available
    this.account = await pool.query(
      `SELECT * FROM customer_tbl WHERE ${this.uniquefieldtoused}`,
      this.uniqueValue
    );
    this.accountData = this.account.rows;

    if (this.accountData.length > 0) {
      this.response = await pool.query(
        `UPDATE customer_tbl SET balance = balance + ${this.amount} WHERE ${this.uniquefieldtoused}`,
        this.uniqueValue
      );
      //    check if the account has been deducted succefully
      if (this.response.rowCount === 1) {
        //   insertind records into the transaction table
        return this.accountData;
      }
    }
    // return this.response;
  }
};
