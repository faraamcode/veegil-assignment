const jwt = require("jsonwebtoken");
exports.verifyUserToken = (req, res, next) => {
  const token = req.body.authorization;
  if (!token) {
    return res.status(403).json({
      message: "no token provided",
    });
  }
  jwt.verify(token, process.env.MY_TOKEN_SECRETE, (err, authData) => {
    if (err) {
      return res.status(500).json({
        message: "invalid token",
      });
    } else {
      req.token = authData;
      next();
    }
  });
};
exports.testUserToken = (req, res, next) => {
  const token = req.body.authorization;
  if (!token) {
    return res.status(403).json({
      access: false,
      message: "no token provided",
    });
  }
  jwt.verify(token, process.env.MY_TOKEN_SECRETE, (err, authData) => {
    if (err) {
      return res.status(500).json({
        access: false,
        message: "invalid token",
      });
    } else {
      return res.status(200).json({
        access: true,
        message: "valid token",
      });
    }
  });
};
